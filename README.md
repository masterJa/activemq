# activemq
spring boot + activemq + zookeeper使用示例，多个consumer监听一个存放Object的队列，通过反序列化，判断存放的对象类型来执行不同的方法。使用zookeeper的curator框架实现分布式锁，来控制一类信息必须等着另一类消息消费完毕才能消费