package cn.mj;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.data.Stat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ActiveMqApplication {
	@Value("${zk_nodes}")
	private String zkNodes;
	
	@Value("${zk_lock_path}")
	private String zkLockPath;


	public static void main(String[] args) {
		SpringApplication.run(ActiveMqApplication.class, args);
	}
	
	@Bean
	public CuratorFramework curatorFramework() {
		CuratorFramework curatorFramework = CuratorFrameworkFactory.builder().connectString(zkNodes).retryPolicy(new ExponentialBackoffRetry(500, 5)).build();
		curatorFramework.start();
		try {
			Stat stat = curatorFramework.checkExists().forPath(zkLockPath);
			System.err.println("stat===" + stat);
			if (stat == null) {
				System.err.println("--------------------" + zkLockPath);
//				curatorFramework.create().withMode(CreateMode.PERSISTENT).forPath(zkLockPath);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
//			curatorFramework.close();
		}
		return curatorFramework;
	}
}
