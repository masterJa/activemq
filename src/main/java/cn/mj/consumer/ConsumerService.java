package cn.mj.consumer;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import cn.mj.entity.User;

@Service
public class ConsumerService {
	
	@JmsListener(destination="str")
	public void receiveMsg(String str) {
		System.out.println("str---" + str);
	}
	
	@JmsListener(destination="obj")
	public void receiveMsg(User user) {
		System.out.println("obj---" + user);
	}
}
