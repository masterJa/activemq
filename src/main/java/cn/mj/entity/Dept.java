package cn.mj.entity;

import java.io.Serializable;

public class Dept implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String name;
	private String code;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "Dept [id=" + id + ", name=" + name + ", code=" + code + "]";
	}

}
