package cn.mj.producer;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

import cn.mj.entity.User;

@Service
public class ProducerService {
	
	@Autowired 
	private JmsTemplate jmsTemplate;
	
	public void sendMsg(String str) {
		Destination destination = new ActiveMQQueue("str");
		jmsTemplate.setDefaultDestination(destination );
		jmsTemplate.send(new MessageCreator() {
			
			@Override
			public Message createMessage(Session session) throws JMSException {
				TextMessage objectMessage = session.createTextMessage(str);
				return objectMessage;
			}
		});
		
	}
	
	public void sendMsg1(User user) {
		Destination destination = new ActiveMQQueue("obj");
		jmsTemplate.setDefaultDestination(destination );
		
		jmsTemplate.convertAndSend(":");
		
		jmsTemplate.send(new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {
				ObjectMessage objectMessage = session.createObjectMessage(user);
				return objectMessage;
			}
		});
		
	}
	
	
	public void sendObjectMsg(Object object) {
		Destination destination = new ActiveMQQueue("object_all");
		jmsTemplate.setDefaultDestination(destination);
		jmsTemplate.convertAndSend(object);
	}
	
}
