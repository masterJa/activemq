package cn.mj.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.mj.entity.Dept;
import cn.mj.entity.User;
import cn.mj.producer.ProducerService;

@RestController
public class OperMsgController {
	@Autowired
	private ProducerService producerService;
	
//	@Autowired
//	private ConsumerService consumerService;
//	
	@GetMapping("/sendMsg")
	public String sendMsg(String user) {
		producerService.sendMsg(user);
		return user;
	}
	
	@GetMapping("/sendMsg1")
	public User sendMsg(User user) {
		System.out.println(user);
		producerService.sendMsg1(user);
		producerService.sendMsg(user.getUsername() + "===" + user.getAge());
		return user;
	}
	
	
//	@GetMapping("/sendMsg")
//	public User receiveMsg() {
//		return consumerService.receiveMsg();
//	}
	
	
	@GetMapping("/user")
	@ResponseBody
	public Object sendUserToMsg(User user) {
		producerService.sendObjectMsg(user);
		return user;
	}
	
	
	@GetMapping("/dept")
	@ResponseBody
	public Object sendDeptToMsg(Dept dept) {
		producerService.sendObjectMsg(dept);
		return dept;
	}
	
	
	
}
