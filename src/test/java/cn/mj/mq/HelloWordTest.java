package cn.mj.mq;

import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQDestination;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;

import cn.mj.ActiveMqApplicationTests;
import cn.mj.entity.User;

public class HelloWordTest extends ActiveMqApplicationTests{
	
	@Autowired
	JmsTemplate jmsTemplate;
	
	@Test
	public void test1() throws Exception {
		System.out.println(jmsTemplate);
	}

	
	@Test
	public void testSendMsg() throws Exception {
		// 创建ActiveMQ连接工厂对象	 "admin","admin","http://127.0.0.1:8161/admin/"
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
//		connectionFactory.setTrustAllPackages(true);
		// 获取连接对象
		Connection connection = connectionFactory.createConnection();
		// 开启连接
		connection.start();
		// 创建session
		// 参数1：transacted - 指出会话是否是事务的
		// 参数2：acknowledgeMode - 指出消费者或者客户端是否会确认所收到的任何消息；如果会话是事务的，则忽略该参数。
		Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
		// 创建连接队列
		Destination destination = session.createQueue("HelloWorld");
		// 创建消息的生产者
		MessageProducer messageProducer = session.createProducer(destination);
		// 发送消息
		for (int i = 1; i <= 3; i++) {
			// 创建文本消息
//			TextMessage textMessage = session.createTextMessage("msg" + i);
			ObjectMessage objectMessage = session.createObjectMessage(new User("姓名" + i, i*10));
			System.out.println("发送消息" + i);
			// 发送消息
//			messageProducer.send(textMessage);
			messageProducer.send(objectMessage);
		}
		// 提交会话
		session.commit();
		//关闭生产者
		messageProducer.close();
		//关闭session
		session.close();
		// 关闭连接
		connection.close();
	}
	
	
	@Test
	public void testReceiveMsg() throws Exception {
		ActiveMQConnectionFactory connectionFactory;
		// Connection ：JMS 客户端到JMS Provider 的连接  
		Connection connection = null;
		// Session： 一个发送或接收消息的线程  
		Session session = null;
		// Destination ：消息的目的地;消息发送给谁.  
		Destination destination = null;
		// 消费者，消息接收者  
		MessageConsumer consumer = null;
		
		connectionFactory = new ActiveMQConnectionFactory("admin", "admin", "tcp://localhost:61616");
		//如果消息存的是ObjectMessage，就必须设置这些为true，信任所以的包
		connectionFactory.setTrustAllPackages(true);
		try {
			// 构造从工厂得到连接对象  
			connection = connectionFactory.createConnection();
			// 启动  
			connection.start();
			// 获取操作连接  
			session = connection.createSession(Boolean.FALSE, Session.AUTO_ACKNOWLEDGE);
			
			destination = session.createQueue("HelloWorld");
			consumer = session.createConsumer(destination);
			consumer.setMessageListener(new MessageListener() {
				@Override
				public void onMessage(Message message) {
					try {
						System.out.println(message);
//						TextMessage textMessage = (TextMessage)message;
//						System.out.println(textMessage.getText());
						ObjectMessage objectMessage = (ObjectMessage) message;
						User user = (User) objectMessage.getObject();
						System.out.println(user);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			while(true){} //防止junit主进程直接关闭导致接收不了消息
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(consumer!=null){
				consumer.close();
			}
			if(session != null){
				session.close();
			}
			if(connection!=null){
				connection.close();
			}
		}
	}
	
	
	
	/**
	 * 删除队列
	 * @throws Exception
	 */
	@Test
	public void testDeleteQueue() throws Exception {
		// 创建ActiveMQ连接工厂对象
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
		// 获取连接对象
		Connection connection = connectionFactory.createConnection();

		// 获取待删除的队列
		ActiveMQDestination deleteDestination = ActiveMQDestination.createDestination("HelloWorld",ActiveMQDestination.QUEUE_TYPE);

		// 删除指定的队列 ActiveMQConnection.destroyDestination(ActiveMQDestination destination)
		System.out.println("deleteDestination-- " + deleteDestination);
		Map<String, String> options = deleteDestination.getOptions();
		System.out.println("options--" + options);
		System.out.println(deleteDestination.getCompositeDestinations());
		if (connection instanceof ActiveMQConnection) {
			ActiveMQConnection activeMQConnection = (ActiveMQConnection) connection;
			activeMQConnection.destroyDestination(deleteDestination);
		}
		connection.close();
	}
	
	
}
